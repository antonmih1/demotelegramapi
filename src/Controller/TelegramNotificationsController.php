<?php

namespace App\Controller;

use App\Entity\TelegramChanel;
use App\Service\TelegramApiService;
use App\Service\TelegramNotificationsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Helper\ResponseHelper;


class TelegramNotificationsController extends AbstractController
{
    private $telegramNotificationsService;
    private $responseHelper;
    private $token;
    private $nameBot;

    public function __construct(TelegramNotificationsService $telegramNotificationsService, ResponseHelper $responseHelper)
    {
        $this->telegramNotificationsService = $telegramNotificationsService;
        $this->responseHelper = $responseHelper;
        $this->token = '1762833924:AAEmTGNRLv0C2vgO5k_TPN_E_xRLf7Vnpaw';
        $this->nameBot = 'investments_api_bot';
    }

    /**
     * @Route("/telegram", name="telegram")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function telegram(Request $request, EntityManagerInterface $entityManager): JsonResponse
    {
        $response = json_decode($request->getContent());
        $data = [
            'status' => 404,
            'message' => 'error message',
        ];

        if (isset($response->my_chat_member->new_chat_member) ) {
            if ($response->my_chat_member->new_chat_member->user->username == $this->nameBot && !in_array($response->my_chat_member->new_chat_member->status, ['left', 'kicked'])) {
                $chat = $response->my_chat_member->chat;
                $rep = $this->getDoctrine()
                    ->getRepository(TelegramChanel::class);
                $telegramChanel = $rep->findOneBy(['chanel_id' => preg_replace("/[^0-9]/", '', $chat->id),]);
                if (!$telegramChanel instanceof TelegramChanel) {
                    $this->telegramNotificationsService->newTelegramChanel($entityManager, $chat);
                }
                $this->telegramNotificationsService->sendNotifications($this->token, $chat);
                $data = [
                    'status' => 200,
                    'message' => 'send notification',
                ];

            }

        }
        return $this->responseHelper->response($data);

    }


}