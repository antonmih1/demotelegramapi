<?php
namespace App\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;

class ResponseHelper
{
    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param $status
     * @param array $headers
     * @return JsonResponse
     */
    public function response($data, $status = 200, $headers = [])
    {
        return new JsonResponse($data, $status, $headers);
    }
}