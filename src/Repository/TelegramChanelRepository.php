<?php

namespace App\Repository;

use App\Entity\TelegramChanel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TelegramChanel|null find($id, $lockMode = null, $lockVersion = null)
 * @method TelegramChanel|null findOneBy(array $criteria, array $orderBy = null)
 * @method TelegramChanel[]    findAll()
 * @method TelegramChanel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TelegramChanelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TelegramChanel::class);
    }

    // /**
    //  * @return TelegramChanel[] Returns an array of TelegramChanel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TelegramChanel
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
