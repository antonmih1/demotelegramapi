<?php
namespace App\Service;

class TelegramApiService
{
    public function sendMessage($token, $message)
    {
        file_get_contents("https://api.telegram.org/bot$token/sendMessage?" . http_build_query($message));
    }

    public function sendPhoto($token, $poll)
    {
        file_get_contents("https://api.telegram.org/bot$token/sendPhoto?" . http_build_query($poll));
    }

    public function sendPoll($token, $photo)
    {
        file_get_contents("https://api.telegram.org/bot$token/sendPoll?" . http_build_query($photo));
    }
}