<?php
namespace App\Service;


use App\Entity\TelegramChanel;
class TelegramNotificationsService
{
    public function __construct(TelegramApiService $telegramApiService)
    {
        $this->telegramApiService = $telegramApiService;
    }

    public function newTelegramChanel($entityManager, $chat)
    {
        $telegramChanel = new TelegramChanel();
        $telegramChanel->setChanelId(preg_replace("/[^0-9]/", '', $chat->id));
        $telegramChanel->setName($chat->title);
        $entityManager->persist($telegramChanel);
        $entityManager->flush();
    }

    public function sendNotifications($token, $chat)
    {
        $message = [
            'text' => 'Добрый день!',
            'chat_id' => $chat->id,
        ];
        $options = array("Да", "Постоянно", "Нет", "Иногда");
        $poll = [
            'chat_id' => $chat->id,
            'question' => 'Вы инвестируете?',
            'options' => json_encode($options),
        ];
        $photoUrl = 'https://i.ibb.co/mCYhcV4/91707.jpg';
        $filename = basename($photoUrl);
        $photoUrl = str_replace($filename, urlencode($filename), $photoUrl);
        $photo = [
            'chat_id' => $chat->id,
            'photo' => $photoUrl,
        ];

        $this->telegramApiService->sendMessage($token, $message);
        $this->telegramApiService->sendPhoto($token, $photo);
        $this->telegramApiService->sendPoll($token, $poll);
    }

}